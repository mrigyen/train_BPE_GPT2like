import jax
import jax.numpy as jnp
from constants import EMBEDDING_LEN


@jax.jit
def magnify(arr: jnp.DeviceArray):
    # take a (device, batch, seq) shaped array and return a (device, batch, seq,
    # 256) shaped array, such that all the tokens are replaced with one-hot
    # vectors of the tokens
    return jnp.where(jnp.arange(EMBEDDING_LEN) == jnp.expand_dims(arr, axis=-1), 1, 0)


@jax.jit
def reduce(arr: jnp.DeviceArray):
    # lossy reduction of (seq_len, encoding_len) to (seq_len, ), taking argmax
    # for every token
    # used mainly for testing
    return jnp.argmax(arr, axis=-1)


def pad_right(arr: jnp.DeviceArray, seq_len: int):
    # arr.shape: (seq_len, )
    clipped_arr = arr[:seq_len]
    pad_amount = max(0, seq_len - len(arr))
    return jnp.pad(clipped_arr, ((0, pad_amount)))


def tokenize(seq: str):
    return jnp.array([c for c in seq.encode("utf-8")])


@jax.jit
def one_hot_encoding(x: int):
    return jnp.where(jnp.arange(EMBEDDING_LEN) == x, 1, 0)


@jax.jit
def one_hot_decoding(arr: jnp.DeviceArray):
    return arr.argmax().astype(int)


def untokenize(arr: jnp.DeviceArray):
    # arr.shape: (seq_len, )
    return bytes(arr.tolist()).decode("utf-8", errors="ignore")


if __name__ == "__main__":
    print(one_hot_encoding(1))
    print(one_hot_decoding(one_hot_encoding(2)))
    tokenized = tokenize("Look behind you.")
    print(tokenized)
    print(tokenized.shape)
    padded = pad_right(tokenized, 100)
    print(padded.shape)
    magnified = magnify(padded)
    print(magnified.shape)
    print(untokenize(padded))
