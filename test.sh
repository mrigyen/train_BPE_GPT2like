#!/usr/bin/env bash

# simulate multi-device to test pmapping and vmapping
export XLA_FLAGS='--xla_force_host_platform_device_count=8'

python3 train.py \
    --root_dir "micro/" \
    --seq_len 16 \
    --dim_att 64 \
    --stack_len 3 \
    --hidden_dim 16 \
   --sub_batch_size 1 \
   --save_path "test.pkl" \
   --architecture "transformer_BPE_arithmetic_modelling"
