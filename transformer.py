import functools
import jax
import jax.numpy as jnp
from jax import random
from jax.nn.initializers import glorot_normal, normal
import einops


@jax.jit
def RoPE(x: jnp.DeviceArray, base: int = 10_000):
    """
    Apply rotary positional embeddings to the input
    reference:
    https://github.com/kingoflolz/mesh-transformer-jax/blob/9d15698694a2a54ff0a32ec5ae4ec05e7a388bee/mesh_transformer/layers.py#L146
    """
    seq_len, encoding_len = x.shape
    theta = 1.0 / (base ** (jnp.arange(0, encoding_len, 2) / encoding_len))
    seq_arange_theta = jnp.einsum("i,j->ij", jnp.arange(seq_len), theta)
    sin = einops.repeat(
        jnp.sin(seq_arange_theta),
        "seq_arange theta -> seq_arange (theta repeat)",
        repeat=2,
    )
    cos = einops.repeat(
        jnp.cos(seq_arange_theta),
        "seq_arange theta -> seq_arange (theta repeat)",
        repeat=2,
    )

    even_embeds = x[:, ::2]
    odd_embeds = x[:, 1::2]
    stacked = jnp.stack((-odd_embeds, even_embeds), axis=-1)
    rotated_every_second_element = einops.rearrange(
        stacked, "seq_len encoding_len r -> seq_len (encoding_len r)"
    )
    return (x * cos) + (rotated_every_second_element * sin)


@functools.partial(jax.jit, static_argnums=(1,))
def layer_norm(
    x: jnp.DeviceArray,
    axis: int,
    eps: float = 1e-5,
) -> jnp.DeviceArray:
    """
    simple layer norm implementation
    ref: https://stackoverflow.com/questions/59830168/layer-normalization-in-pytorch

    x: input jax array
    axis: the axis along which you want layer norm to occur
    """
    # for axis = -1
    # x.shape = (seq_len, X)
    mean = jnp.mean(x, axis=axis, keepdims=True)
    # mean.shape = (seq_len, 1)
    var = jnp.var(x, axis=axis, keepdims=True)
    # var.shape = (seq_len, 1)
    out = (x - mean) / jnp.sqrt(var + eps)
    # out.shape = (seq_len, X)
    return out


def SoftmaxAttention(
    dim_att,
    W_init=glorot_normal(),
    b_init=normal(),
):
    def init_fun(rng, input_shape):
        seq_len, encoding_len = input_shape
        k1, k2, k3, k4 = random.split(rng, 4)
        params_qkv = {
            "weights": W_init(k1, (encoding_len, 3 * dim_att)),
            "biases": b_init(k2, (3 * dim_att,)),
        }
        params_ffn = {
            "weights": W_init(k3, (dim_att, encoding_len)),
            "biases": b_init(k4, (encoding_len,)),
        }
        params = {"qkv": params_qkv, "ffn": params_ffn}
        return input_shape, params

    @jax.jit
    def apply_fun(params, x):
        seq_len, encoding_len = x.shape
        qkv = jnp.dot(x, params["qkv"]["weights"]) + params["qkv"]["biases"]
        q, k, v = jnp.split(qkv, 3, axis=-1)
        w = jnp.einsum("nd,md->nm", q, k)
        w = w / jnp.sqrt(dim_att)
        a = jax.nn.softmax(w, axis=-1)
        # unidirectional
        a = jnp.tril(a)
        x = jnp.einsum("nm,md->nd", a, v)
        out = jnp.dot(x, params["ffn"]["weights"]) + params["ffn"]["biases"]
        return out

    return init_fun, apply_fun


def TransformerBlock(
    seq_len: int,
    dim_att: int,
    hidden_dim: int,
    W_init=glorot_normal(),
    b_init=normal(),
):
    def init_fun(rng, input_shape):
        seq_len, encoding_len = input_shape

        # attention
        init_att, apply_att = SoftmaxAttention(dim_att)
        key, rng = random.split(rng)
        _, params_att = init_att(key, input_shape)

        k1, k2 = random.split(rng)
        params_proj = {
            "weights": W_init(k1, (encoding_len, hidden_dim)),
            "biases": b_init(k2, (hidden_dim,)),
        }
        params_proj2 = {
            "weights": W_init(k1, (hidden_dim, encoding_len)),
            "biases": b_init(k2, (encoding_len,)),
        }
        params = {
            "att": params_att,
            "proj": params_proj,
            "proj2": params_proj2,
        }
        applys = apply_att
        return input_shape, params, applys

    @functools.partial(jax.jit, static_argnums=(1,))
    def apply_fun(params, applys, x):
        # based on GPT-2 architecture
        apply_att = applys

        skip = x
        x = layer_norm(x, (-1,))
        x = RoPE(x)
        x = apply_att(params["att"], x)
        x = x + skip

        skip = x
        x = layer_norm(x, (-1,))
        x = jnp.dot(x, params["proj"]["weights"] + params["proj"]["biases"])
        x = jax.nn.gelu(x)
        x = jnp.dot(x, params["proj2"]["weights"] + params["proj2"]["biases"])
        x = x + skip
        return x

    return init_fun, apply_fun


def TransformerStack(
    dim_att: int,
    stack_len: int,
    hidden_dim: int,
):
    def init_fun(rng, input_shape):
        seq_len, encoding_len = input_shape
        list_params = []
        list_applys = []
        for i in range(stack_len):
            init_block, apply_block = TransformerBlock(seq_len, dim_att, hidden_dim)
            key, rng = random.split(rng)
            input_shape, params, applys = init_block(key, input_shape)
            list_params.append(params)
            list_applys.append((applys, apply_block))
        params = {"transformer_block": list_params}
        return input_shape, params, tuple(list_applys)

    def apply_fun(params, list_applys, x):
        for i in range(stack_len):
            applys, apply_block = list_applys[i]
            x = apply_block(params["transformer_block"][i], applys, x)
        return x

    return init_fun, apply_fun
