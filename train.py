import argparse
import functools
import transformer
from jax import random
import jax_tokenizer as jtokenizer
import jax
import jax.numpy as jnp
import log
import time
from jax.experimental import optimizers
from dataloader import DataLoader
import utils
import constants


parser = argparse.ArgumentParser(description="")
parser.add_argument(
    "--root_dir",
    dest="root_dir",
    metavar="path",
    type=str,
    required=True,
    help="path to directory containing jsonl files used for training",
)
parser.add_argument(
    "--seq_len",
    dest="seq_len",
    type=int,
    help="length of input sequence",
)
parser.add_argument(
    "--dim_att",
    dest="dim_att",
    type=int,
    help="attention dimension",
)
parser.add_argument(
    "--stack_len",
    dest="stack_len",
    type=int,
    help="number of performer blocks you want stacked in the architecture",
)
parser.add_argument(
    "--hidden_dim",
    dest="hidden_dim",
    type=int,
    help="out dimension of first projection in a performer block",
)
parser.add_argument(
    "--sub_batch_size",
    dest="sub_batch_size",
    type=int,
    help="number of samples per device per step",
)
parser.add_argument(
    "--learning_rate",
    dest="learning_rate",
    type=float,
    default=1e-3,
    help="learning rate",
)
parser.add_argument(
    "--save_path",
    dest="save_path",
    metavar="path",
    type=str,
    required=False,
    default="transformer.pkl",
    help="path to save the params as a pickle file",
)
parser.add_argument(
    "--architecture",
    dest="architecture",
    type=str,
    required=True,
    help="specify arch (for futureproofing)",
)
parser.add_argument(
    "--seed",
    dest="seed",
    type=int,
    default=0,
    help="PRNG seed",
)
parser.add_argument(
    "--epochs",
    dest="epochs",
    type=int,
    default=1,
    help="Number of epochs to train for",
)


@functools.partial(jax.vmap, in_axes=(None, None, None, 0, 0))
@functools.partial(jax.jit, static_argnums=(1, 2))
def softmax_cross_entropy(
    params: jnp.DeviceArray,
    apply_model,
    applys,
    prompt: jnp.DeviceArray,
    continuation: jnp.DeviceArray,
):
    """
    params: the parameters of the network, over which you do a gradient descent
    apply_model: the actual forward
    applys: internal forwards
    prompt: (seq_len, encoding_len) input to the model
    continuation: (seq_len, encoding_len) ideal output of the model
    """
    model_logits = apply_model(params, applys, prompt)

    # return the cross entropy of the generated sequence
    return -jnp.sum(continuation * jax.nn.log_softmax(model_logits))


@functools.partial(jax.jit, static_argnums=(1, 2))
def batch_softmax_cross_entropy(params, apply_model, applys, prompts, continuations):
    batch_loss = softmax_cross_entropy(
        params, apply_model, applys, prompts, continuations
    )
    return jnp.average(batch_loss, axis=0)


def train(args):
    rng = random.PRNGKey(args.seed)
    init_fun_adam, update_fun_adam, get_params_adam = optimizers.adam(
        args.learning_rate
    )
    init_model, apply_model = transformer.TransformerStack(
        args.dim_att, args.stack_len, args.hidden_dim
    )

    key, rng = random.split(rng)
    _, params, applys = init_model(key, (args.seq_len, constants.EMBEDDING_LEN))
    opt_state = init_fun_adam(params)

    train_loader = DataLoader(
        args.root_dir + "train/",
        jax.local_device_count(),
        args.sub_batch_size,
        args.seq_len,
    )
    test_loader = DataLoader(
        args.root_dir + "test/",
        jax.local_device_count(),
        args.sub_batch_size,
        args.seq_len,
    )

    local_devices = jax.local_devices()
    replicated_opt_state = jax.device_put_replicated(opt_state, local_devices)
    replicated_params = jax.device_put_replicated(params, local_devices)

    @functools.partial(jax.pmap, axis_name="data", in_axes=(0, 0, 0))
    def pmapped_bsce(params, prompts, continuations):
        return batch_softmax_cross_entropy(
            params,
            apply_model,
            applys,
            prompts,
            continuations,
        )

    def get_testset_accuracy(params, test_loader, once=False):
        total_loss = 0
        for i, (prompts, continuations) in enumerate(test_loader):
            prompts = jtokenizer.magnify(jnp.array(prompts))
            continuations = jtokenizer.magnify(jnp.array(continuations))
            loss = jnp.average(
                pmapped_bsce(
                    params,
                    prompts,
                    continuations,
                )
            )
            total_loss = total_loss + loss
            if once:
                break
        return total_loss

    @functools.partial(jax.pmap, axis_name="data", in_axes=(None, 0, 0, 0, 0))
    def update(step, params, opt_state, prompts, continuations):
        loss, grads = jax.value_and_grad(batch_softmax_cross_entropy)(
            params,
            apply_model,
            applys,
            prompts,
            continuations,
        )
        grads = jax.lax.pmean(grads, axis_name="data")
        loss = jax.lax.pmean(loss, axis_name="data")
        opt_state = update_fun_adam(step, grads, opt_state)
        return get_params_adam(opt_state), loss, opt_state

    broken = False
    start = time.perf_counter()
    for epoch_index in range(args.epochs):
        for i, (prompts, continuations) in enumerate(train_loader):
            if utils.sync_training_end(False):
                broken = True
                break
            prompts = jtokenizer.magnify(jnp.array(prompts))
            continuations = jtokenizer.magnify(jnp.array(continuations))
            replicated_params, loss, replicated_opt_state = update(
                jnp.array(log.step),
                replicated_params,
                replicated_opt_state,
                prompts,
                continuations,
            )
            # the loss and grads across each device is the same
            loss = loss[0]
            end = time.perf_counter()
            scalars = {
                "cross-entropy-loss": loss,
                "perplexity": utils.perplexity(loss),
                "bits-per-character": utils.nat_log_to_binary_log(loss),
                # this is time taken per step, including getting data
                "time": end - start,
                "test_loss": get_testset_accuracy(
                    replicated_params, test_loader, once=True
                ),
            }
            log.scalars(scalars)
            # save a checkpoint after every 1e3 steps
            # include initial generation as a baseline
            if i % 1e3 == 0:
                utils.save_model(
                    args,
                    i,
                    epoch_index,
                    replicated_opt_state,
                )

            start = time.perf_counter()

        if not broken:
            utils.sync_training_end(True)

        # get cross entropy on test set
        log.info(
            f"epoch_test_loss: {get_testset_accuracy(replicated_params, test_loader)}"
        )

        utils.save_model(
            args,
            "EPOCH_COMPLETE",
            epoch_index,
            replicated_opt_state,
        )

    # save a copy of the model after completing training
    utils.save_model(
        args,
        "TRAINING_COMPLETE",
        "TRAINING_COMPLETE",
        replicated_opt_state,
    )


if __name__ == "__main__":
    args = parser.parse_args()
    train(args)
