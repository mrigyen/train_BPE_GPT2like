#!/usr/bin/env bash

export LOG_DIR="alpha/"
mkdir -p $LOG_DIR
python3 train.py \
    --root_dir "dataset/" \
    --seq_len 32 \
    --dim_att 64 \
    --stack_len 3 \
    --hidden_dim 256 \
   --sub_batch_size 32 \
   --save_path $LOG_DIR"BPE.pkl" \
   --architecture "transformer_BPE_arithmetic_modelling"
