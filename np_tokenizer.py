import numpy as np
from numba import jit
from tokenizers import Tokenizer

tokenizer = Tokenizer.from_file("tokenizer.json")


def tokenize(seq: str):
    # return minified tokenized representations of text
    tokenized = tokenizer.encode(seq)
    return np.array(tokenized.ids, dtype=int)


if __name__ == "__main__":
    print(tokenize("2+2=4"))
