import functools
import jax
import jax.numpy as jnp
import pickle
from jax.experimental import optimizers


def save_model(
    args,
    step: int,
    epoch: int,
    replicated_opt_state,
):
    # save a checkpoint
    with open(
        args.save_path + ".epoch_" + str(epoch) + ".step_" + str(step), "wb"
    ) as file:
        opt_state = jax.device_get(jax.tree_map(lambda x: x[0], replicated_opt_state))
        pickle.dump(
            {
                "args": args,
                "opt_state": optimizers.unpack_optimizer_state(opt_state),
            },
            file,
        )


@functools.partial(jax.pmap, axis_name="devices")
def psum(x):
    return jax.lax.psum(x, "devices")


def sync_training_end(ended: bool):
    x = jnp.array([1]) if ended else jnp.array([0])
    xr = jax.device_put_replicated(x, jax.local_devices())
    return jnp.sum(psum(xr)) > 0


@jax.jit
def perplexity(cross_entropy: jnp.DeviceArray):
    return 2 ** nat_log_to_binary_log(cross_entropy)


@jax.jit
def nat_log_to_binary_log(nat_log: jnp.DeviceArray):
    return nat_log / jnp.log(2)
